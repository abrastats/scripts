import fetch from "node-fetch";
import config from "../config/config.js";

export const getPrice = async (tokenId) => {
  try {
    const responseToken = await fetch(
      `${config.coingeckoApiUrl}/coins/${tokenId}`
    );
    const datasToken = await responseToken.json();
    const tokenPrice = datasToken.market_data.current_price.usd;
    return tokenPrice;
  } catch (error) {
    console.log(`Error fetch CG price of ${tokenId} : `, error);
    return 0.0;
  }
};
