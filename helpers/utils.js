import { createRequire } from "module";
import Web3 from "web3";
import RPCs from "../datas/constants/RPCs.js";
import { getPrice } from "./coingecko.js";

const require = createRequire(import.meta.url);
const CauldronABI = require("../datas/abi/CauldronABI.json");
const OracleABI = require("../datas/abi/OracleABI.json");

const web3_eth = new Web3(RPCs.ETH_RPC_URL);
const web3_bsc = new Web3(RPCs.BINANCE_RPC_URL);
const web3_ftm = new Web3(RPCs.FANTOM_RPC_URL);
const web3_arbi = new Web3(RPCs.ARBITRUM_RPC_URL);
const web3_avax = new Web3(RPCs.AVALANCHE_RPC_URL);

export const getWeb3InstanceForChain = (id) => {
  switch (id) {
    default:
    case 1:
      return web3_eth;
    case 56:
      return web3_bsc;
    case 250:
      return web3_ftm;
    case 42161:
      return web3_arbi;
    case 43114:
      return web3_avax;
  }
};

export const getValueObject = async (price, amount) => {
  const ethPrice = await getPrice("ethereum");
  const btcPrice = await getPrice("bitcoin");

  const valueUSD = parseFloat(price) * parseFloat(amount);
  const valueETH =
    parseFloat(parseFloat(price) * parseFloat(amount)) / ethPrice;

  const valueBTC =
    parseFloat(parseFloat(price) * parseFloat(amount)) / btcPrice;
  const decimalsUSD = parseFloat(valueUSD) < 1.0 ? 6 : 2;
  const decimalsETH = parseFloat(valueETH) < 1.0 ? 3 : 2;
  const decimalsBTC = parseFloat(valueBTC) < 1.0 ? 5 : 2;
  return {
    usd: `${parseFloat(valueUSD).toFixed(decimalsUSD)}`,
    eth: `${parseFloat(valueETH).toFixed(decimalsETH)}`,
    btc: `${parseFloat(valueBTC).toFixed(decimalsBTC)}`,
  };
};

export const getValueForDecimals = (value, decimals = 18) => {
  const unit = Object.keys(Web3.utils.unitMap).find(
    (key) =>
      Web3.utils.unitMap[key] ===
      Web3.utils.toBN(10).pow(Web3.utils.toBN(decimals)).toString()
  );
  return Web3.utils.fromWei(`${value}`, unit);
};

export const getOraclePriceOfCauldron = async (cauldron) => {
  const web3 = getWeb3InstanceForChain(1);
  const CauldronContract = new web3.eth.Contract(CauldronABI, cauldron);

  const oracle = await CauldronContract.methods.oracle().call();
  const oracleData = await CauldronContract.methods.oracleData().call();
  const OracleContract = new web3.eth.Contract(OracleABI, oracle);
  const oraclePrice = await OracleContract.methods.get(oracleData).call();

  return oraclePrice["1"];
};
