export const UniswapPoolQuery = `
query($id: ID!, $block: Block_height) { 
  pool(id: $id, block: $block, subgraphError: allow) {
    id
    token0Price
    token1Price
  }
}  
`;
