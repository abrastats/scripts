const PEG_SOURCES = [
  {
    name: "MIM/3Crv",
    chain: 1,
    type: "curve",
    source: {
      mainPool: "0xbEbc44782C7dB0a1A60Cb6fe97d0b483032FF1C7",
      metaPool: "0x5a6a4d54456819380173272a5e8e9b9904bdf41b",
      swapSize: 1000000,
    },
  },
  //   {
  //     name: "MIM CL",
  //     chain: 1,
  //     type: "chainlink",
  //     source: {
  //       url: "",
  //     },
  //   },
];

export default PEG_SOURCES;
