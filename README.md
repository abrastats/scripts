# Scripts

Those scripts have been written to populate and synchronize the Redis database.

On first launch, `npm run populate` will run once, then `npm run synchronize` will run every 15 minutes (called via CRON on the server).

## Run

1. `git clone git@gitlab.com:abrastats/scripts.git`
2. `cd scripts && npm install`
3. In another terminal launch the command `redis-server` if it's not running yet
4. Copy `.env.dist`, rename the copy as `.env.local` and add the API url
5. `npm run populate-dev`
6. `npm run sync-dev`

## Contribute

To contribute to the project, fork the project in your own repository, create a new branch called `feature/description` or `fix/description` and open a merge request for it on the main repo.

If you need to do so, don't forget to create the same branch for the API and the front respositories.

1. Create the `./src/File` that contains your script to run as a `Promise`
2. Complete the `Promise.all([])` logic in `managers/populate.js` and `managers/synchronize.js`
3. Check the `./datas/` folder to add ABIs or Addresses that you would need in your script

Contact Clonescody#1164 on the [Abracadabra Discord](https://discord.gg/pbmftrJ2) for further informations.

## Todo

1. Create the sync() logic
2. Get historical fees per cauldron
3. Refactor code
4. Switch to Typescript

- Get historical TVL with archive node (need an archive node per chain)
