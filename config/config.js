const config = {
  env: process.env.NODE_ENV,
  abracadabraApiUrl: process.env.ABRACADABRA_API_URL,
  coingeckoApiUrl: "https://api.coingecko.com/api/v3",
};

export default config;
