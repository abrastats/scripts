import { createRequire } from "module";
import {
  getValueForDecimals,
  getWeb3InstanceForChain,
} from "../helpers/utils.js";

const require = createRequire(import.meta.url);
const CauldronABI = require("../datas/abi/CauldronABI.json");
const ERC20ABI = require("../datas/abi/ERC20ABI.json");
const BentoBoxABI = require("../datas/abi/BentoBoxABI.json");
const DegenBoxABI = require("../datas/abi/DegenBoxABI.json");

export const getCauldronInfos = async (cauldron) => {
  const { network, address, isDeprecated } = cauldron;
  const web3 = getWeb3InstanceForChain(network);
  const CauldronContract = new web3.eth.Contract(CauldronABI, address);
  const MIMAddress = await CauldronContract.methods.magicInternetMoney().call();

  const getCollateralInfos = new Promise(async (resolve, reject) => {
    const address = await CauldronContract.methods.collateral().call();
    const CollateralContract = new web3.eth.Contract(ERC20ABI, address);
    const name = await CollateralContract.methods.name().call();
    resolve({ collateral: { address, name } });
  });

  const getBentoBoxInfos = new Promise(async (resolve, reject) => {
    const bentoBox = await CauldronContract.methods.bentoBox().call();
    resolve({ bentoBox });
  });

  const getAvailableMimsInfos = new Promise(async (resolve, reject) => {
    if (isDeprecated) {
      resolve({ availableMIMs: "0.00" });
    }
    const bentoBox = await CauldronContract.methods.bentoBox().call();
    const BentoBoxContract = new web3.eth.Contract(BentoBoxABI, bentoBox);
    const availableMIMs = await BentoBoxContract.methods
      .balanceOf(MIMAddress, address)
      .call();
    resolve({
      availableMIMs: parseFloat(web3.utils.fromWei(availableMIMs)).toFixed(2),
    });
  });

  const getBorrowedMimsInfos = new Promise(async (resolve, reject) => {
    const borrowedMIMs = await CauldronContract.methods.totalBorrow().call();
    resolve({
      borrowedMIMs: parseFloat(web3.utils.fromWei(borrowedMIMs["1"])).toFixed(
        2
      ),
    });
  });

  const getWithdrawableAmount = new Promise(async (resolve, reject) => {
    if (isDeprecated) {
      resolve({ withdrawableAmount: "0.00" });
    }
    const web3 = getWeb3InstanceForChain(network);
    const collateral = await CauldronContract.methods.collateral().call();
    const bentoBox = await CauldronContract.methods.bentoBox().call();
    const DegenBoxContract = new web3.eth.Contract(DegenBoxABI, bentoBox);
    const strategy = await DegenBoxContract.methods.strategy(collateral).call();
    if (strategy === "0x0000000000000000000000000000000000000000") {
      resolve({ withdrawableAmount: "0.00" });
    }
    const CollateralContract = new web3.eth.Contract(ERC20ABI, collateral);
    const decimals = await CollateralContract.methods.decimals().call();
    const balance = await CollateralContract.methods.balanceOf(bentoBox).call();
    const withdrawableAmount = getValueForDecimals(balance, decimals);
    resolve({
      withdrawableAmount: `${parseFloat(withdrawableAmount).toFixed(2)}`,
    });
  });

  const getTvl = new Promise(async (resolve, reject) => {
    const totalCollateral = await CauldronContract.methods
      .totalCollateralShare()
      .call();
    const exchangeRate = await CauldronContract.methods.exchangeRate().call();
    console.log(
      "tvl : ",
      parseFloat(
        parseFloat(web3.utils.fromWei(`${totalCollateral}`)) *
          (1 / parseFloat(web3.utils.fromWei(`${exchangeRate}`)))
      ).toFixed(2)
    );
    resolve({
      tvl: parseFloat(
        parseFloat(web3.utils.fromWei(`${totalCollateral}`)) *
          (1 / parseFloat(web3.utils.fromWei(`${exchangeRate}`)))
      ).toFixed(2),
    });
  });

  return Promise.all([
    getCollateralInfos,
    getBentoBoxInfos,
    getAvailableMimsInfos,
    getBorrowedMimsInfos,
    getWithdrawableAmount,
    getTvl,
  ]).then((infos) => {
    return {
      collateral: infos[0].collateral,
      bentoBox: infos[1].bentoBox,
      availableMIMs: infos[2].availableMIMs,
      borrowedMIMs: infos[3].borrowedMIMs,
      withdrawableAmount: infos[4].withdrawableAmount,
      tvl: infos[5].tvl,
    };
  });
};
