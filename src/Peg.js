import { createRequire } from "module";
import { getWeb3InstanceForChain } from "../helpers/utils.js";
import PEG_SOURCES from "../datas/constants/PegSources.js";

const require = createRequire(import.meta.url);
const TriPoolLPABI = require("../datas/abi/TriPoolLPABI.json");
const TriPoolMetaPoolABI = require("../datas/abi/TriPoolMetaPoolABI.json");

// const getPegFromChainlink = async ({ url }) => {};

const getPegAndBalancesFromCurve = async (
  { mainPool, metaPool, swapSize },
  chain = 1
) => {
  const web3 = getWeb3InstanceForChain(chain);
  const PoolContract = new web3.eth.Contract(TriPoolLPABI, mainPool);
  const MetaPoolContract = new web3.eth.Contract(TriPoolMetaPoolABI, metaPool);
  const estimatedOutput = await MetaPoolContract.methods
    .get_dy(0, 1, swapSize)
    .call(); // swapSize MIM swap to the 3pool
  const virtualPriceRaw = await PoolContract.methods.get_virtual_price().call();
  const virtualPrice = web3.utils.fromWei(virtualPriceRaw); // Virtual price of 3Crv underlying

  const peg = parseFloat((estimatedOutput * virtualPrice) / swapSize).toFixed(
    4
  );
  const balances = await MetaPoolContract.methods.get_balances().call();
  return {
    peg,
    reserves: [
      web3.utils.fromWei(balances[0]),
      web3.utils.fromWei(balances[1]),
    ],
  };
};

export const fetchPegs = new Promise(async (resolve, reject) => {
  console.log("Fetching pegs ....");
  let pegs = [];
  await Promise.all(
    PEG_SOURCES.map(async ({ type, chain, source }) => {
      if (type === "curve") {
        const pegAndBalance = await getPegAndBalancesFromCurve(source, chain);
        pegs.push(pegAndBalance);
      }
      //   if (type === "chainlink") {
      //     const chainlinkPeg = await getPegFromChainlink(source);
      //     pegs.push(chainlinkPeg);
      //   }
    })
  );
  console.log("Pegs fetched !");
  resolve({ pegs });
});
